﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BtnClicked ()
    {
        SceneManager.LoadScene("level1");
    }
    public void BtnEscClicked()
    {
        Application.Quit();
    }
    public void ReturnClicked()
    {
        SceneManager.LoadScene("menu");
    }
    public void AuthorsClicked()
    {
        SceneManager.LoadScene("authors");
    }
}
