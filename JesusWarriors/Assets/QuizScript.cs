﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Question{
	public Question(string _question,string [] _variants, int _correct){
		this.questions = _question;
		this.variants = _variants;
		correctAnswer = _correct;
	}

	public string questions;
	public string [] variants;
	public int correctAnswer;

}

public class QuizScript : MonoBehaviour {
	public Question[] questions;
	public GameObject[] labels;
	public GameObject content;
	int activeQuestion = 0;
	bool init=false;
	GameStateScript gameStateScript;

	public AudioClip[] clips;
	private AudioSource speaker;
	Vector3 positionOfNext;

	// Use this for initialization
	void Start () {
		//loadQuestion (0);
		gameStateScript = GetComponent<GameStateScript> ();
		speaker = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (!init) {
			init = true;
			gameStateScript.addPoints(PlayerPrefs.GetInt("score"));
			loadQuestion (0);
		}
	}

	void loadQuestion(int id){
		content.GetComponent<Text> ().text = questions [id].questions;
		labels [0].GetComponent<Text>().text = questions [id].variants[0];
		labels [1].GetComponent<Text> ().text = questions [id].variants[1];
		labels [2].GetComponent<Text> ().text = questions [id].variants [2];
	}

	public void loadNextLevel(){
		SceneManager.LoadScene ("level2");
	}

	public void makeAnswer(int buttonId){
		if (questions [activeQuestion].correctAnswer == buttonId) {
			gameStateScript.addPoints (100);
			speaker.clip = clips [0];
			speaker.Play ();
		} else {
			speaker.clip = clips [1];
			speaker.Play ();
		}

		activeQuestion++;
		if (activeQuestion < questions.Length) {
			loadQuestion (activeQuestion);
		} else {
			labels [0].GetComponent<Text> ().text = "";
			labels [1].GetComponent<Text> ().text = "";
			labels [2].GetComponent<Text> ().text = "";

			GameObject.Find ("Button").GetComponent<Transform> ().position = GameObject.Find ("Answer2Image").GetComponent<Transform> ().position;

			GameObject.Find ("Answer1Image").GetComponent<Transform> ().position = new Vector3 (-10000, 0, 0);
			GameObject.Find ("Answer2Image").GetComponent<Transform> ().position = new Vector3 (-10000, 0, 0);
			GameObject.Find ("Answer3Image").GetComponent<Transform> ().position = new Vector3 (-10000, 0, 0);
			GameObject.Find ("ScoreLabel").GetComponent<Transform> ().position = new Vector3 (-10000, 0, 0);

			content.GetComponent<Text> ().text = "Twój wynik to: " + gameStateScript.scoreCount;
			speaker.clip = clips [2];
			speaker.Play ();
		}
	}
}
