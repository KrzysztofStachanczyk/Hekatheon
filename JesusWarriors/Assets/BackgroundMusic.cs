﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour {
	public AudioClip [] clips;
	AudioSource speaker;
	// Use this for initialization
	void Start () {
		speaker = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!speaker.isPlaying) {
			speaker.clip = clips [Random.Range (0, clips.Length)];
			speaker.Play ();
		}
	}
}
