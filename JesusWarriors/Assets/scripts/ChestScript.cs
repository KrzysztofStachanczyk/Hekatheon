﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestScript : MonoBehaviour {
	public int points = 3;

	Transform playerPos;
	bool opened = false;
	GameStateScript gameScript;
	public GameObject ps ;

	AudioSource speaker;

	// Use this for initialization
	void Start () {
		playerPos = GameObject.Find ("Player").GetComponent<Transform>();
		gameScript = GameObject.Find ("GameState").GetComponent<GameStateScript> ();
		speaker = GetComponent<AudioSource>();
		//ps = GameObject.Find ("Win");
	}

	// Update is called once per frame
	void Update () {
		if (!opened && Input.GetKeyDown (KeyCode.E) && ((transform.position-playerPos.position).magnitude<1)) {
					gameScript.addPoints (points);
					opened = true;
					speaker.Play ();
			if (ps != null) {
				Instantiate (ps, GetComponent<Transform> ().position, Quaternion.identity);
			}
		}

	}
}
