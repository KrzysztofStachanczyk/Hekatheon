﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStateScript : MonoBehaviour {
	public int scoreCount = 0;
	private Text text;
	// Use this for initialization
	void Start () {
		//scoreLabel= GameObject.Find("ScoreLabel");
		text = GameObject.Find("ScoreLabel").GetComponent<Text> ();
		addPoints (0);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadScene ("menu");
		}
	}

	public void addPoints(int scores){
		scoreCount += scores;
		text.text = scoreCount.ToString ();
	}
}
