﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateOpenScript : MonoBehaviour {
	public GameObject gate; 
	public GameObject gate2; 
	AudioSource speaker;
	Transform playerPos;
	Animator animator;

	bool opened=false;

	// Use this for initialization
	void Start () {
		//gameScript = GameObject.Find ("GameState").GetComponent<GameStateScript> ();
		playerPos = GameObject.Find("Player").GetComponent<Transform>();
		animator = GetComponent<Animator> ();
		speaker = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.E) && ((transform.position-playerPos.position).magnitude<1)) {
			//gameScript.addPoints (points);
			opened = !opened;
			animator.SetBool("open",opened);
			speaker.Play ();
			gate.GetComponent<Animator>().SetBool("open",opened);
			gate2.GetComponent<Animator>().SetBool("open",opened);
		}

		if (opened && (playerPos.position - gate.GetComponent<Transform> ().position).magnitude < 1) {
			playerPos.position = gate2.GetComponent<Transform>().position;
		}

	}
}
