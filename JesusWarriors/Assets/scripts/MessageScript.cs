﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageScript : MonoBehaviour {
	Text header;
	Text content;
	Vector3 defaultPosition;

	// Use this for initialization
	void Start () {
		content = GameObject.Find ("MessageText").GetComponent<Text> ();
		header = GameObject.Find ("MessageTitle").GetComponent<Text> ();
		defaultPosition = transform.position;
		transform.position=new Vector3(-1000,0,0);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Backspace)){
			transform.position=new Vector3(-1000,0,0);
		}
	}

	public void showMessage(string title,string msg){
		transform.position = defaultPosition;
		content.text = msg;
		header.text = title;
	}
}
