﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zakonnik1 : MonoBehaviour {
	public float trigDist=1.0f;
	public string message = "";
	public string title = ""; 
	public int points = 1;

	Transform playerPos;
	bool showed;
	MessageScript messageScript;
	GameStateScript gameScript;

	// Use this for initialization
	void Start () {
		playerPos = GameObject.Find ("Player").GetComponent<Transform>();
		messageScript = GameObject.Find ("MessageBox").GetComponent<MessageScript> ();
		gameScript = GameObject.Find ("GameState").GetComponent<GameStateScript> ();
	}
	
	// Update is called once per frame
	void Update () {
		if ((playerPos.position - transform.position).magnitude < trigDist) {
			if (!showed || Input.GetKeyDown (KeyCode.Q)) {
				if (!showed) {
					gameScript.addPoints (points);
				}
				showed = true;
				messageScript.showMessage ( title,message);
			}
		}
	}
}
