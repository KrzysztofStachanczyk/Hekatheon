﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {
	public float horizontalForceFactor=1;
	public float horizontalSpeedLimit=1;
	public float jumpFactor = 1;
	public float groundedHeight = 1;
	public float runThr = 0;
	public string nextSceneName="";
	Vector3 spawnPoint;
	private bool grounded=false;
	private float lx =0;
	Rigidbody2D myRigidBody;
	Animator animator;
	AudioSource speaker;
	string helpText;
	bool helpHidden;


	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		speaker = GetComponent<AudioSource> ();
		spawnPoint = transform.position;
		helpText = GameObject.Find ("Instruction").GetComponent<Text> ().text;
		helpHidden = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		//myRigidBody.AddForce (new Vector2 (Input.GetAxis ("Horizontal") * horizontalForceFactor, 0.0f));

		if (Input.GetAxis ("Horizontal") > runThr) {
			animator.SetBool ("runRight", true);
		} else {
			animator.SetBool ("runRight", false);
		}

		if (Input.GetAxis ("Horizontal") < -runThr) {
			animator.SetBool ("runLeft", true);
		} else {
			animator.SetBool ("runLeft", false);
		}

		if (Input.GetKeyDown (KeyCode.I)) {
			if (helpHidden) {
				helpHidden = false;
				GameObject.Find ("Instruction").GetComponent<Text> ().text = helpText;
			} else {
				GameObject.Find ("Instruction").GetComponent<Text> ().text = "";
				helpHidden = true;
			}
		}

		myRigidBody.velocity = new Vector2(Input.GetAxis ("Horizontal") * horizontalForceFactor,myRigidBody.velocity.y);


		if (Input.GetKeyDown (KeyCode.Space) && grounded) {
			//myRigidBody.velocity = new Vector2 (0.0f, jumpFactor)
			myRigidBody.AddForce (new Vector2 (0.0f, jumpFactor));
			animator.SetBool ("fly", true);
		}

		if (grounded && Mathf.Abs (myRigidBody.velocity.x) > runThr) {
			if (!speaker.isPlaying) {
				speaker.Play ();
			}
		} else {
			speaker.Stop ();
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		Vector3 otherPosition = collision.gameObject.GetComponent<Transform>().position;

		if (collision.gameObject.tag == "Brick" && otherPosition.y<transform.position.y) {
			grounded = true;
			animator.SetBool ("fly", false);
		}

		if (collision.gameObject.tag == "GameOverArea" ) {
			//SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			transform.position=spawnPoint;
		}



	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.gameObject.tag == "SpawnZone" ) {
			//SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			spawnPoint=transform.position;
		}


		if (collision.gameObject.tag == "NextLVL") {
			PlayerPrefs.SetInt("score",GameObject.Find("GameState").GetComponent<GameStateScript>().scoreCount);
			SceneManager.LoadScene (nextSceneName);
		}
	}

	void OnCollisionStay2D(Collision2D collision){
		Vector3 otherPosition = collision.gameObject.GetComponent<Transform>().position;

		if (collision.gameObject.tag == "Brick" && otherPosition.y<transform.position.y) {
			grounded = true;
			animator.SetBool ("fly", false);
		}
	}

	void OnCollisionExit2D(Collision2D collision){
		Vector3 otherPosition = collision.gameObject.GetComponent<Transform>().position;

		if (collision.gameObject.tag == "Brick") {
			grounded = false;
			animator.SetBool ("fly", true);
		}
	}


}
