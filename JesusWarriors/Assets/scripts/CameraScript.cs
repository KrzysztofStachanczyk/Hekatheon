﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
	Transform playerTransform;
	public Vector3 cameraOffset;

	// Use this for initialization
	void Start () {
		playerTransform = GameObject.Find ("Player").GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(playerTransform.position.x,playerTransform.position.y,transform.position.z) + cameraOffset;
	}


}
